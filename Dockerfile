
FROM node:11-alpine

WORKDIR /src

COPY package.json package.json

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]