## Setup

### Setup using docker environment

#### Requirements

* Docker Compose v1.24.1+

#### Setup steps

```
cp env.example .env

docker-compose build
docker-compose up 
``` 

**Web access**

 * Access to site: http://localhost/
 * Initialize Data: http://localhost/init
 * Access to MongoDB Express http://0.0.0.0:8081/db/ukg-middleware/

**Start Middleware without Docker**

 * cp .env.dist .env
 * edit .env and add your enviroment variables
 * yarn or npm install
 * yarn start or npm run start