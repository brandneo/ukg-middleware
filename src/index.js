import express from "express";
import http from "http";
import SocketIO from "socket.io";
import cors from "cors";
import "dotenv/config";

import routes from './routes';
import models, { connectDb } from "./models";

const app = express();
const server = http.Server(app);
const io = new SocketIO(server);
const port = process.env.PORT || 3000;
const eraseDatabaseOnSync = false;

app.use(cors());

app.use('/init', routes.init);
app.use('/activities', routes.activities);
app.use('/users', routes.users);
app.use('/userPresences', routes.userPresences);
app.use('/offers', routes.offers);
app.use('/projects', routes.projects);
app.use('/deals', routes.deals);
app.use('/dealCategories', routes.dealCategories);
app.use('/invoices', routes.invoices);
app.use('/invoicePayments', routes.invoicePayments);
app.use('/companies', routes.companies);

app.get("/", (req, res) => {
  res.send("it's working :)");
});

connectDb().then(async () => {
  if (eraseDatabaseOnSync) {
    await Promise.all([
      models.Users.deleteMany({}),
      models.userPresences.deleteMany({}),
      models.Invoices.deleteMany({}),
      models.invoicePayments.deleteMany({}),
      models.Activities.deleteMany({}),
      models.Projects.deleteMany({}),
      models.dealCategories.deleteMany({}),
      models.Deals.deleteMany({}),
      models.Offers.deleteMany({}),
      models.Companies.deleteMany({})
    ]);
  }
  app.listen(port, () => console.log(`Example app listening on port ${port}!`));
});
