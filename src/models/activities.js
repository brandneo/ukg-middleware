import mongoose from "mongoose";
const activitiesSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  date: {
    type: String
  },
  hours: {
    type: Number
  },
  description: {
    type: String
  },
  billed: {
    type: Boolean
  },
  billable: {
    type: Boolean
  },
  tag: {
    type: Array
  },
  remote_service: {
    type: String
  },
  remote_id: {
    type: String
  },
  remote_url: {
    type: String
  },
  project: {
    type: Object
  },
  task: {
    type: Object
  },
  customer: {
      type: Object
  },
  user: {
    type: Object
  },
  hourly_rate: {
    type: Number
  },
  timer_started_at: {
    type: String
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const Activities = mongoose.model("activities", activitiesSchema);
export default Activities;
