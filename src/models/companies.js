import mongoose from "mongoose";
const companiesSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  type: {
    type: String
  },
  name: {
    type: String
  },
  website: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  },
  fax: {
    type: String
  },
  address: {
    type: String
  },
  tags: {
    type: Array
  },
  user: {
    type: Object
  },
  labels: {
    type: Array
  },
  info: {
    type: String
  },
  custom_properties: {
    type: Object
  },
  identifier: {
      type: String
  },
  intern: {
    type: Boolean
  },
  billing_tax: {
    type: Number
  },
  currency: {
    type: String
  },
  country_code: {
    type: String
  },
  vat_identifier: {
    type: String
  },
  default_invoice_due_days: {
    type: String
  },
  footer: {
    type: String
  },
  projects: {
    type: Array
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const Companies = mongoose.model("companies", companiesSchema);
export default Companies;
