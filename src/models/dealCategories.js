import mongoose from "mongoose";
const dealCategoriesSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  name: {
    type: String
  },
  probability: {
    type: Number
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const dealCategories = mongoose.model("deal-categories", dealCategoriesSchema);
export default dealCategories;
