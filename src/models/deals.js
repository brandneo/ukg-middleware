import mongoose from "mongoose";
const dealsSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  name: {
    type: String
  },
  status: {
    type: String
  },
  reminder_date: {
    type: String
  },
  money: {
    type: Number
  },
  currency: {
    type: String
  },
  info: {
    type: String
  },
  custom_properties: {
    type: Object
  },
  user: {
    type: Object
  },
  company: {
    type: Object
  },
  category: {
    type: Object
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const Deals = mongoose.model("deals", dealsSchema);
export default Deals;
