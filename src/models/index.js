import mongoose from 'mongoose';

import Users from './users';
import userPresences from './userPresences';
import Activities from './activities';
import Offers from './offers';
import Projects from './projects';
import Companies from './companies';
import Deals from './deals';
import dealCategories from './dealCategories';
import Invoices from './invoices';
import invoicePayments from './invoicePayments';

const connectDb = () => {
  return mongoose.connect(process.env.DATABASE_URL,  { useNewUrlParser: true });
};

const models = { Activities, Offers, Projects, Companies, Deals, dealCategories, Invoices, invoicePayments, Users, userPresences };

export { connectDb };

export default models;