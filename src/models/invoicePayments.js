import mongoose from "mongoose";
const invoicePaymentsSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  date: {
    type: String
  },
  invoice: {
    type: Object
  },
  paid_total: {
    type: String
  },
  paid_total_in_account_currency: {
    type: String
  },
  currency: {
    type: String
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const invoicePayments = mongoose.model("invoice-payments", invoicePaymentsSchema);
export default invoicePayments;
