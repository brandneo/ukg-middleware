import mongoose from "mongoose";
const invoicesSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  customer_id: {
    type: Number
  },
  project_id: {
    type: Number
  },
  identifier: {
    type: String
  },
  date: {
    type: String
  },
  due_date: {
    type: String
  },
  service_period: {
    type: String
  },
  service_period_from: {
    type: String
  },
  service_period_to: {
    type: String
  },
  status: {
    type: String
  },
  reversal: {
    type: Boolean
  },
  title: {
    type: String
  },
  recipient_address: {
    type: String
  },
  currency: {
    type: String
  },
  net_total: {
    type: Number
  },
  tax: {
    type: Number
  },
  gross_total: {
    type: Number
  },
  discount: {
    type: Number
  },
  cash_discount: {
    type: Number
  },
  discount: {
    type: Number
  },
  cash_discount_days: {
    type: Number
  },
  created_on: {
    type: String
  },
  updated_on: {
    type: String
  },
  debit_number: {
    type: Number
  },
  credit_number: {
    type: Number
  },
  locked: {
    type: Boolean
  },
  salutation: {
    type: String
  },
  footer: {
    type: String
  },
  items: {
    type: Array
  },
  payments: {
    type: Array
  },
  reminders: {
    type: Array
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const Invoices = mongoose.model("invoices", invoicesSchema);
export default Invoices;
