import mongoose from "mongoose";
const offersSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  identifier: {
    type: String
  },
  date: {
    type: String
  },
  due_date: {
    type: String
  },
  title: {
    type: String
  },
  recipient_address: {
    type: String
  },
  currency: {
    type: String
  },
  net_total: {
    type: Number
  },
  tax: {
    type: Number
  },
  gross_total: {
    type: Number
  },
  discount: {
    type: Number
  },
  cash_discount: {
    type: Number
  },
  discount: {
    type: Number
  },
  created_on: {
    type: String
  },
  updated_on: {
    type: String
  },
  salutation: {
    type: String
  },
  footer: {
    type: String
  },
  company: {
    type: Object
  },
  project: {
    type: Object
  },
  deal: {
    type: Object
  },
  items: {
    type: Array
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const Offers = mongoose.model("offers", offersSchema);
export default Offers;
