import mongoose from "mongoose";
const projectsSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  identifier: {
      type: String
  },
  name: {
    type: String
  },
  active: {
    type: Boolean
  },
  billable: {
    type: Boolean
  },
  finish_date: {
    type: String
  },
  currency: {
    type: String
  },
  billing_variant: {
    type: String
  },
  billing_variant: {
    type: String
  },
  budget: {
    type: Number
  },
  budget_expenses: {
    type: Number
  },
  hourly_rate: {
    type: Number
  },
  info: {
    type: String
  },
  labels: {
    type: Array
  },
  custom_properties: {
    type: Object
  },
  leader: {
    type: Object
  },
  customer: {
    type: Object
  },
  tasks: {
    type: Array
  },
  contracts: {
    type: Array
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const Projects = mongoose.model("projects", projectsSchema);
export default Projects;
