import mongoose from "mongoose";
const userPresenceSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  date: {
    type: String
  },
  from: {
    type: String
  },
  to: {
    type: String
  },
  user: {
    type: Object
  },
  created_at: {
    type: String,
  },
  updated_at: {
    type: String
  },
});

const userPresences = mongoose.model("user-presences", userPresenceSchema);
export default userPresences;
