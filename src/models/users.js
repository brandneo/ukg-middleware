import mongoose from "mongoose";
const userSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  active: {
    type: Boolean
  },
  external: {
    type: Boolean
  },
  email: {
    type: String,
    unique: true
  },
  mobile_phone: {
    type: String
  },
  work_phone: {
    type: String
  },
  home_address: {
    type: String
  },
  info: {
    type: String
  },
  birthday: {
      type: String
  },
  avatar_url: {
    type: String
  },
  custom_properties: {
    type: Object
  },
  unit: {
    type: Object
  },
  created_at: {
    type: String
  },
  updated_at: {
    type: String
  }
});

const Users = mongoose.model("users", userSchema);
export default Users;
