import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.Activities.find({}, function(err, activities) {
      var activitiesMap = {};
  
      activities.forEach(function(activitie) {
        activitiesMap[activitie._id] = activitie;
      });
  
      res.send(activities);
    });
  });

export default router;