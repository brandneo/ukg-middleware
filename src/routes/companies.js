import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.Companies.find({}, function(err, companies) {
      var companiesMap = {};
  
      companies.forEach(function(company) {
        companiesMap[company._id] = company;
      });
  
      res.send(companies);
    });
  });

export default router;