import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.dealCategories.find({}, function(err, dealCategories) {
      var dealCategoriesMap = {};
  
      dealCategories.forEach(function(dealCategorie) {
        dealCategoriesMap[dealCategorie._id] = dealCategorie;
      });
  
      res.send(dealCategories);
    });
  });

export default router;