import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.Deals.find({}, function(err, deals) {
      var dealsMap = {};
  
      deals.forEach(function(deal) {
        dealsMap[deal._id] = deal;
      });
  
      res.send(deals);
    });
  });

export default router;