import activities from "./activities";
import users from "./users";
import userPresences from "./userPresences";
import invoices from "./invoices";
import invoicePayments from "./invoicePayments";
import deals from "./deals";
import dealCategories from "./dealCategories";
import offers from "./offers";
import companies from "./companies";
import projects from "./projects";
import init from "./init";

export default {
  activities,
  users,
  userPresences,
  invoices,
  invoicePayments,
  deals,
  dealCategories,
  offers,
  companies,
  projects,
  init
};
