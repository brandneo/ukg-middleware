import today from "../helpers/getDateToday"
import axios from "axios";
import models from "../models";
import { Router } from 'express';

const router = Router();

const API_PATH = process.env.API_URL;
const API_TIMEZONE = "?from=2020-01-01&to=" + today;

const createDatabaseSets = async (dataSet, dataModel) => {
  let arr = [];
  dataSet.forEach(data => {
    arr.push(new dataModel(data));
  });

  await dataModel.insertMany(arr, function(error, docs) {});
};

const callMocoAPI = (customAPI, outputTotal, urlPage, options, dataModel) => {
  let page = 1;
  let apiCall = setInterval(function() {
    if (outputTotal < 0 || outputTotal == 0) clearInterval(apiCall);
    page++;
    options.url = API_PATH + customAPI + urlPage + page;
    axios(options)
      .then(content => {
        console.log("page " + customAPI, content.data.length);
        outputTotal = outputTotal - content.headers["x-per-page"];
        createDatabaseSets(content.data, dataModel);
      })
      .catch(err => console.log("Error: ", err));
  }, 10000);
};

router.get("/", function(req, res, next) {
  let options = {
    url: API_PATH + "users",
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Token token=" + process.env.API_TOKEN
    }
  };

  let page = 1;
  let urlPage = "?page=";
  let outputTotal = 0;

  //users
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "users");
      if (outputTotal > 0) {
        callMocoAPI("users", outputTotal, "?page=", options, models.Users);
      }
      createDatabaseSets(content.data, models.Users);
    })
    .catch(err => next(err));

  //projects
  options.url = API_PATH + "projects";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "projects");
      if (outputTotal > 0) {
        callMocoAPI(
          "projects",
          outputTotal,
          "?page=",
          options,
          models.Projects
        );
      }
      createDatabaseSets(content.data, models.Projects);
    })
    .catch(err => next(err));

  //deals
  options.url = API_PATH + "deals";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "deals");
      if (outputTotal > 0) {
        callMocoAPI("deals", outputTotal, "?page=", options, models.Deals);
      }
      createDatabaseSets(content.data, models.Deals);
    })
    .catch(err => next(err));

  //companies
  options.url = API_PATH + "companies";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "companies");
      if (outputTotal > 0) {
        callMocoAPI(
          "companies",
          outputTotal,
          "?page=",
          options,
          models.Companies
        );
      }
      createDatabaseSets(content.data, models.Companies);
    })
    .catch(err => next(err));

  //units
  options.url = API_PATH + "units";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "units");

      if (outputTotal > 0) {
        callMocoAPI("units", outputTotal, "?page=", options);
      }
    })
    .catch(err => next(err));

  //offers
  options.url = API_PATH + "offers";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "offers");
      if (outputTotal > 0) {
        callMocoAPI("offers", outputTotal, "?page=", options, models.Offers);
      }
      createDatabaseSets(content.data, models.Offers);
    })
    .catch(err => next(err));

  //invoices/payments
  options.url = API_PATH + "invoices/payments";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "invoices/payments");
      if (outputTotal > 0) {
        callMocoAPI(
          "invoices/payments",
          outputTotal,
          "?page=",
          options,
          models.invoicePayments
        );
      }
      createDatabaseSets(content.data, models.invoicePayments);
    })
    .catch(err => next(err));

  //deal_categories
  options.url = API_PATH + "deal_categories";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "deal_categories");
      if (outputTotal > 0) {
        callMocoAPI(
          "deal_categories",
          outputTotal,
          "?page=",
          options,
          models.dealCategories
        );
      }
      createDatabaseSets(content.data, models.dealCategories);
    })
    .catch(err => next(err));

  //invoices
  options.url = API_PATH + "invoices";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log("URL", API_PATH + "invoices");
      if (outputTotal > 0) {
        callMocoAPI(
          "invoices",
          outputTotal,
          "?page=",
          options,
          models.Invoices
        );
      }
      createDatabaseSets(content.data, models.Invoices);
    })
    .catch(err => next(err));

  //activities
  options.url = API_PATH + "activities" + API_TIMEZONE;
  urlPage = "&page=";
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log(
        "URL",
        API_PATH + "activities" + API_TIMEZONE + urlPage + page
      );
      if (outputTotal > 0) {
        callMocoAPI(
          "activities" + API_TIMEZONE,
          outputTotal,
          "&page=",
          options,
          models.Activities
        );
      }
      createDatabaseSets(content.data, models.Activities);
    })
    .catch(err => next(err));

  //userPresences
  axios(options)
    .then(content => {
      outputTotal = content.headers["x-total"] - content.headers["x-per-page"];
      console.log(
        "URL",
        API_PATH + "users/presences" + API_TIMEZONE + urlPage + page
      );
      if (outputTotal > 0) {
        callMocoAPI(
          "users/presences" + API_TIMEZONE,
          outputTotal,
          "&page=",
          options,
          models.userPresences
        );
      }
      createDatabaseSets(content.data, models.userPresences);
    })
    .catch(err => next(err));

  res.send("Initialize! :)");
});

export default router;