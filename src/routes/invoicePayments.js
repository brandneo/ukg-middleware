import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.invoicePayments.find({}, function(err, invoicePayments) {
      var invoicePaymentsMap = {};
  
      invoicePayments.forEach(function(invoicePayment) {
        invoicePaymentsMap[invoicePayment._id] = invoicePayment;
      });
  
      res.send(invoicePayments);
    });
  });

export default router;