import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.Invoices.find({}, function(err, invoices) {
      var invoicesMap = {};
  
      invoices.forEach(function(invoice) {
        invoicesMap[invoice._id] = invoice;
      });
  
      res.send(invoices);
    });
  });

export default router;