import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.Offers.find({}, function(err, offers) {
      var offersMap = {};
  
      offers.forEach(function(offer) {
        offersMap[offer._id] = offer;
      });
  
      res.send(offers);
    });
  });

export default router;