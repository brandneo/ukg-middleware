import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.Projects.find({}, function(err, projects) {
      var projectsMap = {};
  
      projects.forEach(function(project) {
        projectsMap[project._id] = project;
      });
  
      res.send(projects);
    });
  });

export default router;