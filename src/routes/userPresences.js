import { Router } from 'express';
import models from "../models";

const router = Router();

router.get("/", function(req, res) {
    models.userPresences.find({}, function(err, userPresences) {
      var userPresencesMap = {};
  
      userPresences.forEach(function(userPresence) {
        userPresencesMap[userPresence._id] = userPresence;
      });
  
      res.send(userPresences);
    });
  });

export default router;